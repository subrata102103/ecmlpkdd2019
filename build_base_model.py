# Copyright 2019 Rich Data Corporation Pte Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

'''
Build Transfer Learning experiment Base Models
'''

import os
import sys
import traceback
import getopt

from keras.utils import plot_model

from allcommon import model_generic, build_model_generic, readLendingClubDataFrame, \
    CC_DEBT_CONSOL, YearRange, SMALL_BUS

DIAGNOSTIC = True


def usage():
    print ('Usage: ' + sys.argv[0] + ' startYear endYear models numFolds seedList\n' +
           'Build base model for data from startYear to endYear\n' +
           'models = u-target|u-src|v\n' +
           'seedList = Ex: 7,8,9')
    sys.exit(2)


def main():

    try:
        optlist, args = getopt.getopt(sys.argv[1:], 'hc')

        useCar = False
        for o, a in optlist:
            if o == "-h":
                usage()
            elif o == "-c":
                useCar = True
            else:
                assert False, "unhandled option " + o

        if len(args) != 5:
            usage()

        yearRange = YearRange(int(args[0]), int(args[1]))
        models = args[2]
        n_fold = int(args[3])
        list_of_seeds = [int(s) for s in (args[4]).split(',')]

        verbose_level = 0
        trim_stdev = 10000  # The std deviation for trimming outliers

        n_epochs = 100
        n_cols = 17  # number of columns, the last column is the outcome
        n_row = 100000

        n_batch = 256
        n_nodes = 32
        n_layers = 3  # hidden layers only, aside from 1 for input and 1 for output

        input_model_name = "base_L" + str(n_layers) + "_N" + str(n_nodes) + "_E" + str(n_epochs)
        description = "training on single loan, test on single loan"
        output_model_name = input_model_name

        # model U is the base model
        # model v is transferred using based model
        if models == 'u-target':
            is_using_basemodel = False
            finType = SMALL_BUS
        elif models == 'u-src':
            is_using_basemodel = False
            finType = CC_DEBT_CONSOL
        elif models == 'v':
            # Make sure we don't override u-src output model
            output_model_name = 'v-' + output_model_name
            is_using_basemodel = True
            finType = SMALL_BUS
        else:
            raise Exception ('Unknown models=' + str(models))

        filename = "lending_club_credcard_debtconsol.csv"

        current_model = model_generic
        repeat = len(list_of_seeds)

        cv_fold = str(n_fold) + "fold_" + str(repeat) + "repeat_cv"
        file_png = "model_plot_" + output_model_name + "_" + cv_fold + ".png"
        file_h5 = "model_average_" + output_model_name + "_" + cv_fold + ".h5"
        file_gini = "model_performance_" + output_model_name + "_" + cv_fold + ".csv"

        scriptDir, scriptFilename = os.path.split(os.path.abspath(__file__))
        inputDir = scriptDir + '/input/'

        outputDir = scriptDir + '/output/'
        dataSource = 'credit_debt'
        modelOutputDir = outputDir + 'model/' + dataSource + '/'
        os.makedirs(modelOutputDir, exist_ok = True)

        input_basemodel_filename = "model_average_" + input_model_name + '_' + str(n_fold) + 'fold_' + str(repeat) + 'repeat_cv.h5'
        input_basemodel_fullpath = modelOutputDir + input_basemodel_filename

        output_columns = ['Outcome']
        onehot_features = []
        numeric_features = ['revol_util_n',
                            'int_rate_n',
                            'installment_n',
                            'tot_hi_cred_lim_n',
                            'emp_length_n',
                            'dti_n',
                            'avg_cur_bal_n',
                            'all_util_n',
                            'acc_open_past_24mths_n',
                            'annual_inc_n',
                            'loan_amnt_n',
                            'cover'
                            ]
        ordinal_features = ['Outcome']
        opt_trim_features = numeric_features
        optDf = readLendingClubDataFrame(inputDir, yearRange, finType, useCar)
        model, performance = build_model_generic(cv_fold,
                                                 n_epochs,
                                                 n_row,
                                                 n_fold,
                                                 n_batch,
                                                 trim_stdev,
                                                 verbose_level,
                                                 description,
                                                 filename,
                                                 n_nodes,
                                                 current_model,
                                                 n_cols,
                                                 n_layers,
                                                 input_model_name,
                                                 output_model_name,
                                                 list_of_seeds,

                                                 inputDir,
                                                 outputDir,
                                                 output_columns,
                                                 onehot_features,
                                                 ordinal_features,
                                                 opt_trim_features,

                                                 is_using_basemodel,
                                                 input_basemodel_fullpath,
                                                 optDf = optDf
                                                 )

        plot_model(model, to_file = outputDir + file_png, show_shapes = True)
        model.save(modelOutputDir + file_h5)
        performance.to_csv(outputDir + file_gini, sep = ',', index = False)
        text = "  Number of rows:" + str(n_row) + "  Average Gini of " + str(n_fold) + " fold x " + str(repeat) + " repeat cross validation experiments is " + str(performance.loc[:, "gini"].mean()) + " with Standard Deviation:" + str(performance.loc[:, "gini"].std())
        print(text)
        print("Summary of final output model:")
        print(model.summary())

    except getopt.GetoptError as err:
        print (str(err))
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (err))

    except Exception as e:
        # print help information and exit:
        print (traceback.format_exc())
        sys.exit('Failed: %s' % (e))


# Run main() if explicitly loaded (as opposed to being imported)
if __name__ == "__main__":
    main()

